<?php
/*
WP Post Template: News
*/
?>
<?php get_header(); ?>
<div class="container">
<div class="main">
<div class="content">
	<div class="row">
    <?php get_template_part( '/inc/parts/content', 'news' ); ?>
    <?php get_sidebar(); ?>
  </div><!--row-->
</div><!--content-->
<?php get_footer(); ?>