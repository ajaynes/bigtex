<div class="col-xs-12 col-md-3 col-md-pull-9">
    <aside class="cat">
        <?php if ( is_active_sidebar( 'category' ) ) : ?>
            <?php dynamic_sidebar( 'category' ); ?>
        <?php endif; ?>
    </aside>
</div>