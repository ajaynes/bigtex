<?php
/*
Template Name: Tips
*/
?>
<?php get_header(); ?>
<div class="container">
  <div class="main">
    <div class="content">
    <div class="row">
    <?php get_template_part( '/inc/parts/content', 'tips' ); ?>
    <?php get_sidebar(); ?>
    </div>
</div>
<?php get_footer(); ?>