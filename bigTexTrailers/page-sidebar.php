<?php
/*
Template Name: Page with Sidebar
*/
?>
<?php get_header(); ?>
<div class="container">
<div class="main">
<div class="content">
	<div class="row">
    <?php get_template_part( '/inc/parts/content', 'pagewithsidebar' ); ?>
    <?php get_sidebar(); ?>
  </div><!--row-->
</div><!--content-->
<?php get_footer(); ?>