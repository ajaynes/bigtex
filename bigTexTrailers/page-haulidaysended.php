<?php
/*
Template Name: Haulidays Ended
*/
?>
<?php get_header();?>
         
    <div class="container">
        
            <div class="content">
                <div class="row">
                     <?php get_template_part( '/inc/parts/content', 'hauliday' ); ?>    
                </div><!--row-->
            </div><!--content-->
        
    </div><!--container-->

<?php get_footer(); ?>