<div class="col-xs-12 col-md-3 col-md-pull-9">
    <aside class="mainsidebar">
        <?php if ( is_active_sidebar( 'main' ) ) : ?>
            <?php dynamic_sidebar( 'main' ); ?>
        <?php endif; ?>
    </aside>
</div>