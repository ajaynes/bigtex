<?php
/*
Template Name: Become a Dealer
*/
?>
<?php get_header(); ?>
<div class="container">
<div class="main">
<div class="content">
	<div class="row">
    <?php get_template_part( '/inc/parts/content', 'become' ); ?>
  </div><!--row-->
</div><!--content-->
<?php get_footer(); ?>