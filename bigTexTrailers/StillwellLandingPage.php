<?php
/*
Template Name: Stillwell Landing page
*/
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
	<!--[if lte IE 8 ]> <html <?php language_attributes(); ?> class="ie8"> <![endif]-->
	<head>
		<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>">
		<title><?php bloginfo('name'); ?> <?php wp_title(); ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
		<?php wp_head(); ?>
	</head>
	<body <?php body_class(); ?>>
      
    	<a href="#" class="go-top hidden-xs"><span class="fa fa-arrow-circle-o-up fa-2x fa-fw"></span></a>
    	
		<div class="StillBlackDiv">
			<div class="col-sm-12">
			    <img class="img-responsive StillLogo" src="/wp-content/uploads/2017/03/logo-e1490913510903.png">
		    </div>
		</div>
    	
    	<div class="row StillBgimage">
          <div class="StillBgimage">  
             <div class="col-sm-2">
			</div>
             <div class="col-sm-8">
                 <div class="StlVideo"><iframe src="https://www.youtube.com/embed/2Nk19eGAmb0" width="1120" height="630" frameborder="0" allowfullscreen="allowfullscreen"></iframe></div>		
			 </div>
			 <div class="col-sm-2">
			 </div>
			</div> 
		</div>
     <div class="StillWhiteBack">  
        <div class="row" "StillWhiteBack">
			 <div class="col-sm-1">
			 </div>
             <div class="col-sm-2">
				 <a href="http://bigtextrailers.com"><img class="img-responsive" src="/wp-content/uploads/2017/03/home-button-e1490814171714.png"></a>
			 </div>
       		 <div class="col-sm-6">

				<div class="Middleimage2"><img class="img-responsive" src="/wp-content/uploads/2017/03/middleimage2-1.png"></div>
			
			 </div>
             <div class="col-sm-2">
						<a href="http://bigtextrailers.com/14lx-tandem-axle-low-profile-extra-wide-dump/" class="css_btn_class">Click Here to See the 14LX</a>			 
             </div>
             <div class="col-sm-1">
			 </div>
        </div>         

	</div>
       
       <div class="Midsection3">
        <div class="row">
			<div class="col-sm-12">
				<img class="img-responsive" src="/wp-content/uploads/2017/03/3image.jpg">
			</div>
        </div>
	</div>  
    <div class="StillWhiteBack">    
        <div class="row">
        	<div class="col-sm-2">
			</div>
            <div class="col-sm-8">
                   <?php get_template_part( '/inc/parts/content', 'page' ); ?>
			</div>
            <div class="col-sm-2">
			</div>
		</div>
	</div>

<!--browser update-->
<script> 
var $buoop = {c:2}; 
function $buo_f(){ 
 var e = document.createElement("script"); 
 e.src = "//browser-update.org/update.min.js"; 
 document.body.appendChild(e);
};
try {document.addEventListener("DOMContentLoaded", $buo_f,false)}
catch(e){window.attachEvent("onload", $buo_f)}
</script> 

<!--pixel thing-->
<script type="text/javascript">
adroll_adv_id = "7PDXI4TUQFAGFJKDRMOBRX";
adroll_pix_id = "FDGGWORBSRE7TNM6EBWN5S";
// adroll_email = "username@example.com"; // OPTIONAL: provide email to improve user identification
(function () {
var _onload = function(){
if (document.readyState && !/loaded|complete/.test(document.readyState)){setTimeout(_onload, 10);return}
if (!window.__adroll_loaded){__adroll_loaded=true;setTimeout(_onload, 50);return}
var scr = document.createElement("script");
var host = (("https:" == document.location.protocol) ? "https://s.adroll.com" : "http://a.adroll.com");
scr.setAttribute('async', 'true');
scr.type = "text/javascript";
scr.src = host + "/j/roundtrip.js";
((document.getElementsByTagName('head') || [null])[0] ||
document.getElementsByTagName('script')[0].parentNode).appendChild(scr);
};
if (window.addEventListener) {window.addEventListener('load', _onload, false);}
else {window.attachEvent('onload', _onload)}
}());
</script> 

<!-- Google Code for Remarketing Tag -->
<!--------------------------------------------------
Remarketing tags may not be associated with personally identifiable information or placed on pages related to sensitive categories. See more information and instructions on how to setup the tag on: http://google.com/ads/remarketingsetup
--------------------------------------------------->
<script type="text/javascript">
var google_tag_params = {
dynx_itemid: 'REPLACE_WITH_VALUE',
dynx_itemid2: 'REPLACE_WITH_VALUE',
dynx_pagetype: 'REPLACE_WITH_VALUE',
dynx_totalvalue: 'REPLACE_WITH_VALUE',
};
</script>
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 867936049;
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/867936049/?guid=ON&amp;script=0"/>
</div>
</noscript>


		<?php wp_footer(); ?>

	</body>
</html>