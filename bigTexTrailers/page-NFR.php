<?php
/*
Template Name: NFR Homepage
*/
?>

<?php get_header();?>
<div class="forcefullwidth_wrapper_tp_banner">
<img src="http://bigtextrailers.com/wp-content/uploads/2016/07/sweepstakes1-2500.png" class="img-responsive center-block" alt="Responsive image">
</div>
<div class="nfrUp">
<div class="container">
<div class="row nfrFeat">
    <div class="col-xs-12 col-md-4"><a href="/22gn-tandem-dual-axle-gooseneck/" title="22GN HD Tandem Dual Axle" target=_blank"><img src="/wp-content/uploads/2016/07/22GN.png"/></a></div>
    <div class="col-xs-12 col-md-4"><a href="/14et-tandem-axle-equipment/" title="14ET Havey Duty Tandem Axle" target=_blank"><img src="/wp-content/uploads/2016/07/14ET.png"/></a></div>
    <div class="col-xs-12 col-md-4"><a href="/14lx-tandem-axle-low-profile-extra-wide-dump/" title="14lx tandem axle low profile wide dump" target=_blank"><img src="/wp-content/uploads/2016/07/14LX.png"/></a></div>
</div>
</div>
</div>
<div class="forcefullwidth_wrapper_tp_banner">
<img src="http://bigtextrailers.com/wp-content/uploads/2016/07/slideBT-2500.png" class="img-responsive center-block" alt="Responsive image">
</div>
<div class="nfrLow">
<div class="container">
<div class="row nfrAbout">
    <div class="col-xs-12 col-md-6"><a href="/about-us/" title="Big Tex Trailers" target=_blank"><img src="/wp-content/uploads/2016/07/about-bt.png"/></a></div>
    <div class="col-xs-12 col-md-6"><a href="http://www.nfrexperience.com/" title="Big Tex at NFR Finals" target="_target"><img src="/wp-content/uploads/2016/07/event-location.png"/></a></div>
</div>
<div class="row nfrBull">
    <div class="col-xs-12 col-md-4"><a href="https://www.facebook.com/Winn-Ratliff-Professional-Bareback-Rider-321744947946783" title="Winn Ratliff" target="_blank"><img src="/wp-content/uploads/2016/08/winn_ratliff.png"/></a></div>
    <div class="col-xs-12 col-md-4"><a href="https://www.facebook.com/HeithDeMoss/" title="Heith DeMoss" target="_target"><img src="/wp-content/uploads/2016/07/heith-demoss.png"/></div>
    <div class="col-xs-12 col-md-4"><a href="https://www.facebook.com/Sarah-Rose-McDonald-Bling-1482401028711750/" title="Sarah Rose McDonald" target="_blank"><img src="/wp-content/uploads/2016/07/sarah-mcdonald.png"/></a></div>
</div>
</div>
</div>
<div class="container">
<div class="homepage">
	<div class="row">
    <?php get_template_part( '/inc/parts/content', 'nfr' ); ?>
  </div><!--row-->
<?php get_footer(); ?>