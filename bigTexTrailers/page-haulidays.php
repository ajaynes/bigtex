<?php
/*
Template Name: Haulidays Landing Page
*/
?>
<div class="white-body">
    <img src="http://bigtextrailers.com/wp-content/uploads/2016/11/holidays-landing-header.png" class="img-responsive center-block" alt="Haulidays">
    <?php get_template_part( '/inc/parts/content', 'headerhauliday' ); ?>    
    <div class="container">
        <div class="mainholiday">
            <div class="content">
                <div class="row">
                     <?php get_template_part( '/inc/parts/content', 'hauliday' ); ?>    
                </div><!--row-->
            </div><!--content-->
        </div><!--mainholiday-->
    </div><!--container-->
</div><!--whitebody-->
<?php get_footer(); ?>