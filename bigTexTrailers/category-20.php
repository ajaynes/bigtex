<?php get_header();?>
	<div class="container">
	<div class="main">
	<div class="content">
		<div class="row">
        <div class="col-xs-12">
          <article>
              <h2 class="bt text-center"><?php printf( __( '%s', 'twentyten' ), single_cat_title( '', false )  );?></h2>
              <?php
                $category_description = category_description();
                if ( ! empty( $category_description ) )
                echo '<div class="archive-meta">' . $category_description . '</div>';
                get_template_part( '/inc/parts/content', 'category' );
              ?>
              <div class="row">
                <?php while ( have_posts() ) : the_post();
                  get_template_part( 'content', 'options' );
                  endwhile;
                ?>
               <?php /*?> <ul class="pager bt">
                	<li class="pull-left"><?php previous_posts_link('&laquo; Previous Trailers') ?></li>
                  <li class="pull-right"><?php next_posts_link('Next Trailers &raquo;','') ?></li>
                </ul><?php */?>
              </div><!--row-->
            </article>
        </div><!--col-xs-12 col-md-9-->
		</div><!--content-->
    </div>
<?php get_footer(); ?>