<?php
/**
 * A unique identifier is defined to store the options in the database and reference them from the theme.
 * By default it uses the theme name, in lowercase and without spaces, but this can be changed if needed.
 * If the identifier changes, it'll appear as if the options have been reset.
 */

function optionsframework_option_name() {

	// This gets the theme name from the stylesheet
	$themename = wp_get_theme();
	$themename = preg_replace("/\W/", "_", strtolower($themename) );

	$optionsframework_settings = get_option( 'optionsframework' );
	$optionsframework_settings['id'] = $themename;
	update_option( 'optionsframework', $optionsframework_settings );
}

/**
 * Defines an array of options that will be used to generate the settings page and be saved in the database.
 * When creating the 'id' fields, make sure to use all lowercase and no spaces.
 *
 * If you are making your theme translatable, you should replace 'options_framework_theme'
 * with the actual text domain for your theme.  Read more:
 * http://codex.wordpress.org/Function_Reference/load_theme_textdomain
 */

function optionsframework_options() {

	// Test data
	$test_array = array(
		'one' => __('One', 'options_framework_theme'),
		'two' => __('Two', 'options_framework_theme'),
		'three' => __('Three', 'options_framework_theme'),
		'four' => __('Four', 'options_framework_theme'),
		'five' => __('Five', 'options_framework_theme')
	);

	// Multicheck Array
	$multicheck_array = array(
		'one' => __('French Toast', 'options_framework_theme'),
		'two' => __('Pancake', 'options_framework_theme'),
		'three' => __('Omelette', 'options_framework_theme'),
		'four' => __('Crepe', 'options_framework_theme'),
		'five' => __('Waffle', 'options_framework_theme')
	);

	// Multicheck Defaults
	$multicheck_defaults = array(
		'one' => '1',
		'five' => '1'
	);

	// Background Defaults
	$background_defaults = array(
		'color' => '',
		'image' => '',
		'repeat' => 'repeat',
		'position' => 'top center',
		'attachment'=>'scroll' );

	// Typography Defaults
	$typography_defaults = array(
		'size' => '15px',
		'face' => 'georgia',
		'style' => 'bold',
		'color' => '#bada55' );
		
	// Typography Options
	$typography_options = array(
		'sizes' => array( '6','12','14','16','20' ),
		'faces' => array( 'Helvetica Neue' => 'Helvetica Neue','Arial' => 'Arial' ),
		'styles' => array( 'normal' => 'Normal','bold' => 'Bold' ),
		'color' => false
	);

	// Pull all the categories into an array
	$options_categories = array();
	$options_categories_obj = get_categories();
	foreach ($options_categories_obj as $category) {
		$options_categories[$category->cat_ID] = $category->cat_name;
	}
	
	// Pull all tags into an array
	$options_tags = array();
	$options_tags_obj = get_tags();
	foreach ( $options_tags_obj as $tag ) {
		$options_tags[$tag->term_id] = $tag->name;
	}


	// Pull all the pages into an array
	$options_pages = array();
	$options_pages_obj = get_pages('sort_column=post_parent,menu_order');
	$options_pages[''] = 'Select a page:';
	foreach ($options_pages_obj as $page) {
		$options_pages[$page->ID] = $page->post_title;
	}

	// If using image radio buttons, define a directory path
	$imagepath =  get_template_directory_uri() . '/images/';

	$options = array();
	
	$options[] = array(
		'name' => __('Theme Options', 'options_framework_theme'),
		'type' => 'heading');
				
		$options[] = array(
		'name' => __('Address Line 1', 'options_framework_theme'),
		'desc' => __('Line one of the theme address.', 'options_framework_theme'),
		'id' => 'address1',
		'std' => '100 Main Street,',
		'type' => 'text');
		
		$options[] = array(
		'name' => __('Address Line 2', 'options_framework_theme'),
		'desc' => __('Line two of the theme address.', 'options_framework_theme'),
		'id' => 'address2',
		'std' => 'Anywhere US 12345',
		'type' => 'text');
		
		$options[] = array(
		'name' => __('Phone', 'options_framework_theme'),
		'desc' => __('Phone Number', 'options_framework_theme'),
		'id' => 'phone1',
		'std' => '555.555.5555',
		'type' => 'text');

		$options[] = array(
		'name' => __('Footer bar', 'options_framework_theme'),
		'desc' => __('Upload an image for the footer logo bar.', 'options_framework_theme'),
		'id' => 'footer_uploader',
		'type' => 'upload');
		
	$options[] = array(
		'name' => __('Slider Options', 'options_framework_theme'),
		'type' => 'heading');
		
		$options[] = array(
		'name' => __('Image Slide 1', 'options_framework_theme'),
		'desc' => __('First slider image.', 'options_framework_theme'),
		'id' => 'slide1',
		'type' => 'upload');
		
		$options[] = array(
		'name' => __('Caption Header 1', 'options_framework_theme'),
		'desc' => __('Caption Title for slide 1', 'options_framework_theme'),
		'id' => 'captitle1',
		'std' => '',
		'type' => 'text');
		
		$options[] = array(
		'name' => __('Caption Text 1', 'options_framework_theme'),
		'desc' => __('Caption text for slide 1', 'options_framework_theme'),
		'id' => 'captext1',
		'std' => '',
		'type' => 'textarea');
		
		$options[] = array(
		'name' => __('Image Slide 2', 'options_framework_theme'),
		'desc' => __('Second slider image.', 'options_framework_theme'),
		'id' => 'slide2',
		'type' => 'upload');
		
		$options[] = array(
		'name' => __('Caption Header 2', 'options_framework_theme'),
		'desc' => __('Caption Title for slide 2', 'options_framework_theme'),
		'id' => 'captitle2',
		'std' => '',
		'type' => 'text');
		
		$options[] = array(
		'name' => __('Caption Text 2', 'options_framework_theme'),
		'desc' => __('Caption text for slide 2', 'options_framework_theme'),
		'id' => 'captext2',
		'std' => '',
		'type' => 'textarea');
		
		$options[] = array(
		'name' => __('Image Slide 3', 'options_framework_theme'),
		'desc' => __('Third slider image.', 'options_framework_theme'),
		'id' => 'slide3',
		'type' => 'upload');
		
		$options[] = array(
		'name' => __('Caption Header 3', 'options_framework_theme'),
		'desc' => __('Caption Title for slide 3', 'options_framework_theme'),
		'id' => 'captitle3',
		'std' => '',
		'type' => 'text');
		
		$options[] = array(
		'name' => __('Caption Text 3', 'options_framework_theme'),
		'desc' => __('Caption text for slide 3', 'options_framework_theme'),
		'id' => 'captext3',
		'std' => '',
		'type' => 'textarea');
		
		$options[] = array(
		'name' => __('Image Slide 4', 'options_framework_theme'),
		'desc' => __('Fourth slider image.', 'options_framework_theme'),
		'id' => 'slide4',
		'type' => 'upload');
		
		$options[] = array(
		'name' => __('Caption Header 4', 'options_framework_theme'),
		'desc' => __('Caption Title for slide 4', 'options_framework_theme'),
		'id' => 'captitle4',
		'std' => '',
		'type' => 'text');
		
		$options[] = array(
		'name' => __('Caption Text 4', 'options_framework_theme'),
		'desc' => __('Caption text for slide 4', 'options_framework_theme'),
		'id' => 'captext4',
		'std' => '',
		'type' => 'textarea');
		
		$options[] = array(
		'name' => __('Image Slide 5', 'options_framework_theme'),
		'desc' => __('Fifth slider image.', 'options_framework_theme'),
		'id' => 'slide5',
		'type' => 'upload');
		
		$options[] = array(
		'name' => __('Caption Header 5', 'options_framework_theme'),
		'desc' => __('Caption Title for slide 5', 'options_framework_theme'),
		'id' => 'captitle5',
		'std' => '',
		'type' => 'text');
		
		$options[] = array(
		'name' => __('Caption Text 5', 'options_framework_theme'),
		'desc' => __('Caption text for slide 5', 'options_framework_theme'),
		'id' => 'captext5',
		'std' => '',
		'type' => 'textarea');
		
		$options[] = array(
		'name' => __('Image Slide 6', 'options_framework_theme'),
		'desc' => __('Sixth slider image.', 'options_framework_theme'),
		'id' => 'slide6',
		'type' => 'upload');
		
		$options[] = array(
		'name' => __('Caption Header 6', 'options_framework_theme'),
		'desc' => __('Caption Title for slide 6', 'options_framework_theme'),
		'id' => 'captitle6',
		'std' => '',
		'type' => 'text');
		
		$options[] = array(
		'name' => __('Caption Text 6', 'options_framework_theme'),
		'desc' => __('Caption text for slide 6', 'options_framework_theme'),
		'id' => 'captext6',
		'std' => '',
		'type' => 'textarea');

	return $options;
}