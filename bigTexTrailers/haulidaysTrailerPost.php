<?php
/*
WP Post Template: Haulidays Post
*/
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
	<!--[if lte IE 8 ]> <html <?php language_attributes(); ?> class="ie8"> <![endif]-->
	<head>
		<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>">
		<title><?php bloginfo('name'); ?> <?php wp_title(); ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
		<?php wp_head(); ?>
	</head>
	<body <?php body_class(); ?>>
  		<?php get_template_part( 'livechat' ); ?> 
      <?php get_template_part( 'warranty' ); ?>
      <div><?php get_template_part( 'haulidaze' ); ?> </div>
    	<a href="#" class="go-top hidden-xs"><span class="fa fa-arrow-circle-o-up fa-2x fa-fw"></span></a>
		<header>
    	<?php get_template_part( '/inc/parts/menu', 'nav' ); ?>      
		</header> 
<div class="container">
<div class="main">
<div class="content">
	<div class="row">
    <?php get_template_part( '/inc/parts/content', 'haulidaytrailerpost' ); ?>
  </div><!--row-->
</div><!--content-->
<?php get_footer(); ?>