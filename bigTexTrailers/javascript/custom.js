jQuery(document).ready(function(){
	jQuery('#slider').carousel();
	jQuery('#homeSlider').carousel({interval:6000});
	jQuery("#menu-social a, #menu-social-1 a").each(function () {
    jQuery(this).html(jQuery(this).text(''));
	});
	jQuery('#menu-social .youtube a, #menu-social-1 .youtube a').append('<span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x"></i><i class="fa fa-youtube fa-stack-1x fa-inverse"></i></span>');
	jQuery('#menu-social .google a, #menu-social-1 .google a').append('<span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x"></i><i class="fa fa-google-plus fa-stack-1x fa-inverse"></i></span>');
	jQuery('#menu-social .facebook a, #menu-social-1 .facebook a').append('<span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x"></i><i class="fa fa-facebook fa-stack-1x fa-inverse"></i></span>');
	jQuery('#menu-social .twitter a, #menu-social-1 .twitter a').append('<span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x"></i><i class="fa fa-twitter fa-stack-1x fa-inverse"></i></span>');
	jQuery('#menu-social .instagram a, #menu-social-1 .instagram a').append('<span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x"></i><i class="fa fa-instagram fa-stack-1x fa-inverse"></i></span>');
	jQuery('<li class="devider">&nbsp</li>').insertAfter('.navbar-nav > li');
	jQuery('.devider').last().hide();
/*	jQuery('.carousel-caption:not(:has(h4))').hide();*/
	//remove the ability to use "p align="
	jQuery('*').each(function(){
		jQuery(this).removeAttr('align');
	});
	// Show or hide the sticky footer button
	jQuery(window).scroll(function(){
		if (jQuery(this).scrollTop() > 200){
			jQuery('.go-top').fadeIn(200);
		} else{
			jQuery('.go-top').fadeOut(200);
		}
	});
	// Animate the scroll to top
	jQuery('.go-top').click(function(event){
		event.preventDefault();
		jQuery('html, body').animate({scrollTop: 0}, 300);
	});
		jQuery('.bt.trailers h3, .single h2.bt.post').each(function() {
		var html=jQuery(this).html();
		var word = html.substr(0, html.indexOf(" "));
		var rest = html.substr(html.indexOf(" "));
		jQuery(this).html(rest).prepend(jQuery("<span/>").html(word).addClass("yellow"));
	});
		jQuery('.widget_text h2.widgettitle').html(function(){	
			// separate the text by spaces
			var text= jQuery(this).text().split(' ');
			// drop the last word and store it in a variable
			var last = text.pop();
			// join the text back and if it has more than 1 word add the span tag
			// to the last word
			return text.join(" ") + (text.length > 0 ? ' <span class="yellow">'+last+'</span>' : last);
	});
	jQuery('.line a').append(' <span class="fa fa-download"></span>');
	jQuery('#wpcf7-f713-p12-o1 form, #wpcf7-f1267-p1243-o1').addClass('form-horizontal');
	jQuery('.btoptions').each(function(i){
		if(i%2===0){
			jQuery(this).nextAll().andSelf().slice(0,2).wrapAll('<div class="row optionrow"></div>');
		}
	});
	jQuery('.single table.table.table-condensed').wrap('<div class="table-responsive"></div>');
	jQuery(".bt.trailers h3:contains('HD'), .single h2.bt.post:contains('HD')").html(function(_, html) {
   return html.replace(/(HD)/g, '<span class="red">$1</span>');
	});
	jQuery(".gform_wrapper .organicweb-readonly input").attr("readonly", "");
	jQuery('.gfield_contains_required.hidden_label .ginput_container label').append('<span class="gfield_required">*</span>');
	jQuery('.gform_button').addClass('btn btn-lg btn-block btn-primary');
	jQuery('#gform_browse_button_6_61').addClass('btn btn-primary');
	/*jQuery('#wr360trailerviewer').addClass('btn btn-primary btn-lg');*/
	jQuery('a#wr360placer_wr360PlayerId20').css('color', ' ');
	jQuery('#gform_wrapper_4 form').addClass('form-inline');
	jQuery('#gform_wrapper_4 .gform_button').removeClass('btn-block');
	jQuery('.gv-table-view').addClass('table table-striped bt');
	jQuery('#facebook-likebox-2').addClass('hidden-md');
	jQuery('.category-all-trailers:not(.paged) .col-xs-12.bt.trailers:first').prepend('<div class="ribbon"><span>New Product</span></div>');
	jQuery(".clickrow").click(function() {
        window.document.location = jQuery(this).data("href");
    });
	 jQuery('.fsSubmit.fsPagination input').addClass('btn btn-lg btn-primary');
	jQuery('.fsPagination .fsPreviousButton').addClass('btn btn-lg btn-primary');
	jQuery('.fsPagination .fsNextButton').addClass('btn btn-lg btn-primary');
});