<?php
/*
Template Name: Homepage
*/
?>

<?php get_header();?>
<div class="hidden-xs">
<?php putRevSlider("bigtex") ?>
</div>
<div class="container">
<div class="homepage">
	<div class="row">
    <?php get_template_part( '/inc/parts/content', 'home' ); ?>
  </div><!--row-->
<?php get_footer(); ?>