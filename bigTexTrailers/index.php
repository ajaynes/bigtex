<?php get_header(); ?>
<div class="container">
<div class="content">
	<div class="row">
		<?php get_sidebar(); ?>
		<?php get_template_part( '/inc/parts/content', 'index' ); ?>
 </div>
</div>
<?php get_footer(); ?>