<div id="slider" class="carousel slide">
	<div class="carousel-inner">
    	<div class="item active">
			<?php if ( of_get_option('slide1') ) { 
                echo '<img src="'; echo of_get_option('slide1'); echo '" alt="Slide 1" />';
            } 
			else { echo '<img src="'; echo bloginfo('stylesheet_directory'); echo '/images/slides/utility-trailers.jpg" alt="Utility Trailer" />';
			}?>
            <div class="carousel-caption">
            	<?php if ( of_get_option('captitle1') ) { ?>
                	<h4><?php echo of_get_option('captitle1'); ?></h4>
                <?php } ?>
                <?php if ( of_get_option('captext1') ) { ?>
                	<p><?php echo of_get_option('captext1'); ?></p>
                <?php } ?>
            </div><!--carousel-caption-->
        </div>
        <!--item-->
        
        <div class="item">
            <?php if ( of_get_option('slide2') ) { 
                echo '<img src="'; echo of_get_option('slide2'); echo '" alt="Slide 2" />';
            } 
			else { echo '<img src="'; echo bloginfo('stylesheet_directory'); echo '/images/slides/dump-trailer.jpg" alt="Dump Trailer" />';
			}?>
            <div class="carousel-caption">
            	<?php if ( of_get_option('captitle2') ) { ?>
                	<h4><?php echo of_get_option('captitle2'); ?></h4>
                <?php } ?>
                <?php if ( of_get_option('captext2') ) { ?>
                	<p><?php echo of_get_option('captext2'); ?></p>
                <?php } ?>
            </div><!--carousel-caption-->
        </div>
        <!--item-->
        
        <div class="item">
			<?php if ( of_get_option('slide3') ) { 
                echo '<img src="'; echo of_get_option('slide3'); echo '" alt="Slide 3" />';
            } 
			else { echo '<img src="'; echo bloginfo('stylesheet_directory'); echo '/images/slides/gooseneck-trailers.jpg" alt="Gooseneck Trailers" />';
			}?>
            <div class="carousel-caption">
            	<?php if ( of_get_option('captitle3') ) { ?>
                	<h4><?php echo of_get_option('captitle3'); ?></h4>
                <?php } ?>
                <?php if ( of_get_option('captext3') ) { ?>
                	<p><?php echo of_get_option('captext3'); ?></p>
                <?php } ?>
            </div><!--carousel-caption-->
        </div>
        <!--item-->
        
        <?php if ( of_get_option('slide4') ) { ?>
        	<div class="item">
        		<img src="<?php echo of_get_option('slide4'); ?>" alt="Slide 4" />
                <div class="carousel-caption">
					<?php if ( of_get_option('captitle4') ) { ?>
                        <h4><?php echo of_get_option('captitle4'); ?></h4>
                    <?php } ?>
                    <?php if ( of_get_option('captext4') ) { ?>
                        <p><?php echo of_get_option('captext4'); ?></p>
                    <?php } ?>
                </div><!--carousel-caption-->
        	</div>
        <?php }?>
        <!--item-->
        
        <?php if ( of_get_option('slide5') ) { ?>
        	<div class="item">
        		<img src="<?php echo of_get_option('slide5'); ?>" alt="Slide 5" />
                <div class="carousel-caption">
					<?php if ( of_get_option('captitle5') ) { ?>
                        <h4><?php echo of_get_option('captitle5'); ?></h4>
                    <?php } ?>
                    <?php if ( of_get_option('captext5') ) { ?>
                        <p><?php echo of_get_option('captext5'); ?></p>
                    <?php } ?>
                </div><!--carousel-caption-->
        	</div>
        <?php }?>
        <!--item-->
        
        <?php if ( of_get_option('slide6') ) { ?>
        	<div class="item">
        		<img src="<?php echo of_get_option('slide6'); ?>" alt="Slide 6" />
                <div class="carousel-caption">
					<?php if ( of_get_option('captitle6') ) { ?>
                        <h4><?php echo of_get_option('captitle6'); ?></h4>
                    <?php } ?>
                    <?php if ( of_get_option('captext6') ) { ?>
                        <p><?php echo of_get_option('captext6'); ?></p>
                    <?php } ?>
                </div><!--carousel-caption-->
        	</div>
        <?php }?>
        <!--item-->
        
    </div><!--carousel inner-->   
	
	
	
	
	
	
	
	
	
	<?php /*?><ol class="carousel-indicators">
        <li data-target="#slider" data-slide-to="0" class="active"></li>
        <li data-target="#slider" data-slide-to="1"></li>
        <li data-target="#slider" data-slide-to="2"></li>
        <li data-target="#slider" data-slide-to="3"></li>
        <li data-target="#slider" data-slide-to="4"></li>
        <li data-target="#slider" data-slide-to="5"></li>
    </ol>
	<div class="carousel-inner">
    	<div class="item active"><img src="<?php bloginfo('stylesheet_directory'); ?>/images/slides/utility-trailers.jpg" width="960" height="351" alt="Horse Trailer" /></div>
        <div class="item"><img src="<?php bloginfo('stylesheet_directory'); ?>/images/slides/truck-beds.jpg" width="960" height="351" alt="Dump Trailer" /></div>
        <div class="item"><img src="<?php bloginfo('stylesheet_directory'); ?>/images/slides/stock-trailer.jpg" width="960" height="351" alt="Stock Trailer" /></div>
        <div class="item"><img src="<?php bloginfo('stylesheet_directory'); ?>/images/slides/gooseneck-trailers.jpg" width="960" height="351" alt="Utility Trailers" /></div>
        <div class="item"><img src="<?php bloginfo('stylesheet_directory'); ?>/images/slides/dump-trailer.jpg" width="960" height="351" alt="Gooseneck Trailers" /></div>
        <div class="item"><img src="<?php bloginfo('stylesheet_directory'); ?>/images/slides/bumper-pull-horse-trailer.jpg" width="960" height="351" alt="Truck Beds" /></div>
    </div><?php */?>
</div><!--slider-->