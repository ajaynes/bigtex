<div class="col-xs-12 col-md-9 col-md-push-3">
    <article>
        <h2 class="subheader"><?php the_title(); ?></h2>
        <?php while ( have_posts() ) : the_post(); ?>
            <?php the_content(); ?>
            <?php edit_post_link('<i class="fa fa-pencil"></i> Edit'); ?>
            <div class="locator-embed">
  				<script type="text/javascript">(function(){var scr=document.createElement("script"),s=document.getElementsByTagName("script")[0];scr.id="zsl-script";scr.type="text/javascript";scr.async=true;scr.src="https://cdn.zenlocator.com/B6xOk1xp.js";s.parentNode.insertBefore(scr,s)})();</script><div id="zen-store-locator"></div>
			</div>
        <?php endwhile; // end of the loop. ?>	
    </article>
</div><!--col-xs-8-->