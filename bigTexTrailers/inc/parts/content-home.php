<div class="col-xs-12">
    <?php get_template_part( 'slider', 'home' ); ?>
    <?php get_template_part( 'newsletter' ); ?>
    <?php /*?><h2><?php the_title(); ?></h2><?php */?>
    <?php while ( have_posts() ) : the_post(); ?>
				<?php the_content(); ?>
        <?php edit_post_link('<i class="fa fa-pencil"></i> Edit'); ?>
    <?php endwhile; // end of the loop. ?>
</div><!--col-xs-12-->