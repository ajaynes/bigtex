<!DOCTYPE html>
<html <?php language_attributes(); ?>>
	<!--[if lte IE 8 ]> <html <?php language_attributes(); ?> class="ie8"> <![endif]-->
	<head>
		<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>">
		<title><?php bloginfo('name'); ?> <?php wp_title(); ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
		<?php wp_head(); ?>
	</head>
	<body <?php body_class(); ?>>      
    	<a href="#" class="go-top hidden-xs"><span class="fa fa-arrow-circle-o-up fa-2x fa-fw"></span></a>
		<header>
    	      
		</header>        