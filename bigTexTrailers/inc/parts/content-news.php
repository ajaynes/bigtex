<div class="col-xs-12 col-md-9 col-md-push-3">
	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
    <h2><?php the_title(); ?></h2>
    <p>Posted on <?php the_time('F jS, Y'); ?></p>
    <?php the_content(); ?>  
	<?php endwhile; else: ?>
    <p>Sorry, no posts matched your criteria.</p>
  <?php endif; ?>
</div>