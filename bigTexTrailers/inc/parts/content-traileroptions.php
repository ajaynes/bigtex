<div class="col-md-9 col-md-push-3">
  <div class="bt options">
    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
    <h2 class="bt text-center">
      <?php the_title(); ?>
    </h2>
    <div class="row">
      <div class="col-sm-6">
        <?php the_post_thumbnail('full', array('class' => 'img-responsive')); ?>
      </div><!--col-sm-6-->
      <div class="col-sm-6">
        <?php the_content(); ?>
      </div><!--col-sm-6--> 
    </div><!--row-->
    <?php endwhile; else: ?>
    <p>Sorry, no posts matched your criteria.</p>
    <?php endif; ?>
  </div><!--bt options-->
</div><!--col-md-9 col-md-push-3-->