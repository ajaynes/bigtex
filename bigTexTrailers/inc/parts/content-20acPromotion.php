<div class="col-xs-12">
	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
        	<div class="row">
          	<div class="col-md-3">
          	<h2 class="bt post text-center"><?php the_title(); ?></h2>
          	<a href="#"><img class="img-responsive wp-image-3862 aligncenter" src="/wp-content/uploads/2017/04/instant-rebate.png" alt="$1000 Instant Rebate" /></a>
          	<a href="#"><img class="img-responsive wp-image-3862 aligncenter" src="/wp-content/uploads/2017/04/find-a-dealer-for-more-info.png" alt="$1000 Instant Rebate" /></a>  
				<div class="play-video"><a title="20AC" href="https://youtu.be/IfaE_o9qJXQ?list=PLheWtsfKGbAk926HCuyYvkP-03fkf3Vgp" target="_blank"><img class="img-responsive wp-image-3862 aligncenter" src="/wp-content/uploads/2017/04/YouTube-Play-Button.png" alt="20AC YouTube" /></a> </div>       	
          	</div>
            <div class="col-md-9">
                <h3 class="bt text-center">Standard Features</h3>
                <div class="row">
                	<?php the_content('Read more on "'.the_title('', '', false).'" &raquo;'); ?>
                </div>
            </div>
          </div>          
          <div class="row">         
            <div class="col-md-11 col-md-offset-1">
            	<?php the_field('middle'); ?>
            </div>
          </div>
          <div class="row">
          	<div class="col-xs-12">
            	<?php get_template_part('disclaimer'); ?>
            </div>
          </div>
          <div class="row">
          	<div class="col-md-5 col-md-offset-1">
            	<div class="postInfo">
          			<?php the_field('left_info'); ?>
              </div>
            </div>
            <div class="col-md-5">
            	<div class="postInfo">
          			<?php the_field('right_info'); ?>
              </div>
            </div>
          </div>
        <hr class="grayBreaker" />
        <div class="row">
            <div class="col-md-4">
                <h4 class="subheader">Specs</h4>
            </div>
        </div>
        <div class="row">
            <div class="col-md-11 col-md-offset-1 line">
                <?php the_field('line_drawing'); ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
            	<?php the_field('model_weight_description_table'); ?>
            </div>
            <div class="col-md-6">
            	<?php the_field('dimensions'); ?>
            </div>
        </div>
        <div class="row">
          	<div class="col-xs-12">
            	<?php get_template_part('weights'); ?>
            </div>
          </div>
        <div class="row">
            <div class="col-md-4">
                <h4 class="subheader">Options</h4>
            </div>
        </div>
	<div class="row">
          	<div class="col-xs-12">
            	<?php the_field('options'); ?>
            </div>
          </div>
	<div class="row">
            <div class="col-md-4">
                <h4 class="subheader">3D</h4>
            </div>
        </div>
	<div class="row">
          	<div class="col-xs-12">
            	<?php the_field('3d_rotation'); ?>
            </div>
          </div>

        
        <?php wp_link_pages(array('before' => '<p><strong>Pages:</strong> ', 'after' => '</p>', 'next_or_number' => 'number')); ?>
		<?php the_tags( '<p>Tags: ', ', ', '</p>'); ?>
        
       
    <?php endwhile; else: ?>
    	<p>Sorry, no posts matched your criteria.</p>
    <?php endif; ?>
</div>