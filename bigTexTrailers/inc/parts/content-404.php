<div class="col-xs-12 col-md-9 col-md-push-3">
    <article>
        <h2 class="subheader">Oh No! Page not found :(</h2>
        <p>Please return to the <a href="<?php bloginfo('url'); ?>/">homepage</a>.</p>
        <a href="<?php bloginfo('url'); ?>/"><img src="<?php bloginfo('stylesheet_directory'); ?>/images/404.jpg" alt="404" /></a>
    </article>
</div><!--col-xs-8-->