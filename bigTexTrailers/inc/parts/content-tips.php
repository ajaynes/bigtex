<div class="col-xs-12 col-md-9 col-md-push-3">
		<?php global $more; ?>
		<?php query_posts(array('cat' => '25')); ?>
    <?php if (have_posts()) : ?>
    <?php $more = 0;
    while (have_posts()) : the_post(); ?>
    <section class="post">
    	<h2><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>
      <div class="row">
				<div class="col-xs-12 col-md-4"><?php the_post_thumbnail('large', array('class'=>'img-responsive bordered'));?></div>
        <div class="col-xs-12 col-md-8"><?php the_excerpt();?> <a href="<?php echo get_permalink(); ?>"> Read More...</a></div>
      </div>
      <p><?php edit_post_link('<i class="fa fa-pencil"></i> Edit'); ?></p>
		</section>
    
		<?php endwhile; 
    wp_reset_query(); ?>

    <nav>
      <?php posts_nav_link('&nbsp;&bull;&nbsp;'); ?>
    </nav>

		<?php else : ?>

    <section class="post">
      <h2>Not Found</h2>
      <p>Sorry, but the requested resource was not found on this site.</p>
    </section>

    <?php endif; ?>
</div>