<div class="col-xs-8">
    <article>
        <h3><?php the_title(); ?></h3>
        	<section class="well">
				<?php while ( have_posts() ) : the_post(); ?>
                    <?php the_content(); ?>
                    <?php edit_post_link('edit', '<span class="edit-link pull-right btn btn-default"><i class="fa fa-pencil"></i>', '</span>'); ?>
                <?php endwhile; // end of the loop. ?>
            </section><!--well-->	
    </article>
</div><!--col-xs-8-->