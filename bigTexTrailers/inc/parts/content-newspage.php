<div class="col-xs-12 col-md-9 col-md-push-3">
    <article>
        <?php $queryBT = new WP_Query( 'cat=14' ); ?>
        <?php if ($queryBT->have_posts()) : ?>
        <?php while ( $queryBT->have_posts() ) : $queryBT->the_post(); ?>
        <section class="post news">
          <div class="row">
            <div class="col-xs-12 col-md-4">
            	 <?php if ( has_post_thumbnail() ) {
							the_post_thumbnail('large', array( 'class' => 'img-responsive bordered' ) );
							} else { ?>
							<img src="<?php bloginfo('template_directory'); ?>/images/defaultFeatured.jpg" alt="<?php the_title(); ?>" class="img-responsive bordered" />
						<?php } ?>
						</div>
            <div class="col-xs-12 col-md-8">
							<h2><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>
              <p>Posted on <?php the_time('F jS, Y'); ?></p>
							<?php the_excerpt('&raquo; &raquo; &raquo; &raquo;'); ?>
            </div>
        	</div>
          <p><?php edit_post_link('<i class="fa fa-pencil"></i> Edit'); ?></p>
        </section>
        <?php endwhile; // end of the loop. ?>	
    </article>
    		<ul class="pager bt">
          <li class="pull-left"><?php previous_posts_link('&laquo; Older') ?></li>
          <li class="pull-right"><?php next_posts_link('Newer &raquo;','') ?></li>
        </ul>
      <?php else : ?>
      <section class="post">
          <h2>Not Found</h2>
          <p>Sorry, but the requested resource was not found on this site.</p>
      </section>
  
      <?php endif; ?>
</div><!--col-xs-8-->