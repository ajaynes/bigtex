<div class="col-xs-12 col-md-6">
  <article>
  	<h2 class="subheader">Contact Us</h2>
    <p>We welcome comments, questions or feedback about Big Tex Trailers on the form below. For a quick response, please enter your question and information below, and we will get back to you within 1 business day or less.</p>
		<p>For questions about purchasing a Big Tex Trailer or where to locate a dealer, <a title="Locate a Dealer" href="http://www.bigtextrailers.com/locate-a-dealer/">please click here</a>.</p>
		<p>For questions about becoming a Big Tex Dealer, <a title="Become A Dealer" href="http://www.bigtextrailers.com/become-a-dealer/">please click here</a>.</p>
		<!--<p>This form is <em><strong>NOT</strong></em> for questions about dealers, parts or what trailer best fits your needs.  These are questions that are better addressed at <a title="Ask Big Tex" href="http://www.bigtextrailers.com/ask-big-tex/">AskBigTex</a>.</p>-->
		<h2><span class="fa fa-comment"></span> Comments, Questions or Feedback</h2>
    <?php gravity_form(3, false, false); ?>
  </article>
</div>
<div class="col-xs-12 col-md-6">
  <article>
  	<?php
				$post_id = 685;
				$queried_post = get_post($post_id);
		?>
    <h2 class="subheader"><?php echo $queried_post->post_title; ?></h2>
    <?php echo $queried_post->post_content; ?>      
  </article>
</div>