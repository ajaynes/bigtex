<div class="col-xs-12 col-md-9 col-md-push-3">
    <article>
        <h2 class="subheader"><?php the_title(); ?></h2>
        <?php while ( have_posts() ) : the_post(); ?>
            <?php the_content(); ?>
            <?php edit_post_link('<i class="fa fa-pencil"></i> Edit'); ?>
        <?php endwhile; // end of the loop. ?>	
    </article>
</div><!--col-xs-8-->