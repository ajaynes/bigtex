<div class="container">
	<?php wp_nav_menu(array(
			'theme_location' => 'top',
			'container' => 'nav',
			'menu_class' => 'list-inline list-unstyled pull-right hidden-xs hidden-sm',
			'fallback_cb' => false
		));
	?>
	<div class="slogan pull-right hidden-xs hidden-sm">America's BEST Selling Professional Grade Trailers</div>
	<nav class="navbar navbar-default bt" role="navigation">
  	<div class="navbar-header">
    <a class="navbar-brand" href="<?php bloginfo('url'); ?>/">
    	<img src="<?php bloginfo('stylesheet_directory'); ?>/images/bigTexLogo2.png" width="222" height="103" alt="Big Tex Trailers Logo" class="hidden-sm" />
      <img src="<?php bloginfo('stylesheet_directory'); ?>/images/bigTexLogo-sm.png" width="180" height="84" alt="Big Tex Trailers Logo" class="hidden-lg hidden-md hidden-xs" />
    </a>
    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".collapser">
      <span class="sr-only">Toggle navigation</span>
      <span class="fa fa-bars"></span>
    </button>
	</div>
		<?php wp_nav_menu( array(
        'menu'      		=> 'main',
        'depth'     		=> 2,
        'container'  		=> 'div',
        'container_class'   => 'collapse navbar-collapse collapser bt',
        'menu_class'        => 'nav navbar-nav navbar-right',
        'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
        'walker'            => new wp_bootstrap_navwalker())
      );
    ?>
	</nav>
</div>