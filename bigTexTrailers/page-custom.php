<?php
/*
Template Name: Custom Page
*/
?>
<?php get_header(); ?>
<div class="content">
	<div class="row">
		<?php get_sidebar(); ?>
        <?php get_template_part( '/inc/parts/content', 'custom' ); ?>
    </div><!--row-fluid-->
</div><!--content-->
<?php get_footer(); ?>