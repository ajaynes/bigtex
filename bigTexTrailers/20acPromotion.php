<?php
/*
WP Post Template: 20AC Promotion
*/
?>
<?php get_header(); ?>
<div class="container">
<div class="main">
<div class="content">
	<div class="row">
    <?php get_template_part( '/inc/parts/content', '20acPromotion' ); ?>
  </div><!--row-->
</div><!--content-->
<?php get_footer(); ?>