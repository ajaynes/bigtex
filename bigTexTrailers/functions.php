<?php
//testing this out to fix the permalinks issue - AJaynes
function prefix_setup_schedule() {
	if ( ! wp_next_scheduled( 'prefix_hourly_event' ) ) {
		wp_schedule_event( time(), 'hourly', 'prefix_hourly_event');
	}
}
add_action( 'wp', 'prefix_setup_schedule' );

function prefix_do_this_hourly() {
	flush_rewrite_rules( true );
}
add_action( 'prefix_hourly_event', 'prefix_do_this_hourly' );
//end test code

add_filter( 'gform_disable_notification', 'disable_notification', 10, 4 );
function disable_notification( $is_disabled, $notification, $form, $entry ) {

    //There is no concept of admin notifications anymore, so we will need to disable notifications based on other criteria such as name
    if ( $notification['name'] == 'Warranty Notification' ) {
        return true;
    }

    return $is_disabled;
}
/*custom code for the paid membership pro plugin*/
function my_pmpro_has_membership_access_filter($access, $post, $user)
{
if(!empty($user->membership_level) && $user->membership_level->ID == 1)
return true; //level 1 ALWAYS has access
return $access;
}
add_filter("pmpro_has_membership_access_filter", "my_pmpro_has_membership_access_filter", 10, 3); 
function new_excerpt_more( $more ) {
	return ' <a class="read-more" href="'. get_permalink( get_the_ID() ) . '">' . __('Read More...', 'your-text-domain') . '</a>';
}
add_filter( 'excerpt_more', 'new_excerpt_more' );
function custom_excerpt_length( $length ) {
	return 60;
}
add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );
//custom post
// Register Custom Post Type
/*function news() {
	$labels = array(
		'name'                => _x( 'News Events', 'Post Type General Name', 'text_domain' ),
		'singular_name'       => _x( 'News', 'Post Type Singular Name', 'text_domain' ),
		'menu_name'           => __( 'News Items', 'text_domain' ),
		'parent_item_colon'   => __( 'Parent Item:', 'text_domain' ),
		'all_items'           => __( 'All Items', 'text_domain' ),
		'view_item'           => __( 'View Item', 'text_domain' ),
		'add_new_item'        => __( 'Add New Item', 'text_domain' ),
		'add_new'             => __( 'Add New', 'text_domain' ),
		'edit_item'           => __( 'Edit Item', 'text_domain' ),
		'update_item'         => __( 'Update Item', 'text_domain' ),
		'search_items'        => __( 'Search Item', 'text_domain' ),
		'not_found'           => __( 'Not found', 'text_domain' ),
		'not_found_in_trash'  => __( 'Not found in Trash', 'text_domain' ),
	);
	$args = array(
		'label'               => __( 'news', 'text_domain' ),
		'description'         => __( 'News Events', 'text_domain' ),
		'labels'              => $labels,
		'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', 'revisions', 'custom-fields', ),
		'taxonomies'          => array( 'category', 'post_tag' ),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 5,
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'post',
	);
	register_post_type( 'news', $args );
}
// Hook into the 'init' action
add_action( 'init', 'news', 0 );
function testimonials() {
	$labels = array(
		'name'                => _x( 'Testimonials', 'Post Type General Name', 'text_domain' ),
		'singular_name'       => _x( 'Testimonial', 'Post Type Singular Name', 'text_domain' ),
		'menu_name'           => __( 'Testimonials', 'text_domain' ),
		'parent_item_colon'   => __( 'Parent Item:', 'text_domain' ),
		'all_items'           => __( 'All Items', 'text_domain' ),
		'view_item'           => __( 'View Item', 'text_domain' ),
		'add_new_item'        => __( 'Add New Item', 'text_domain' ),
		'add_new'             => __( 'Add New', 'text_domain' ),
		'edit_item'           => __( 'Edit Item', 'text_domain' ),
		'update_item'         => __( 'Update Item', 'text_domain' ),
		'search_items'        => __( 'Search Item', 'text_domain' ),
		'not_found'           => __( 'Not found', 'text_domain' ),
		'not_found_in_trash'  => __( 'Not found in Trash', 'text_domain' ),
	);
	$args = array(
		'label'               => __( 'testimonials', 'text_domain' ),
		'description'         => __( 'Testimonials', 'text_domain' ),
		'labels'              => $labels,
		'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', 'revisions', 'custom-fields', ),
		'taxonomies'          => array( 'category', 'post_tag' ),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 6,
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'post',
	);
	register_post_type( 'testimonials', $args );
}
// Hook into the 'init' action
add_action( 'init', 'testimonials', 0 );*/
add_theme_support( 'post-thumbnails' );
add_image_size('featuredImageCropped', 370, 225, true);
add_filter( 'post_thumbnail_html', 'my_post_image_html', 10, 3 );
function my_post_image_html( $html, $post_id, $post_image_id ) {
  $html = '<a href="' . get_permalink( $post_id ) . '" title="' . esc_attr( get_post_field( 'post_title', $post_id ) ) . '">' . $html . '</a>';
  return $html;
}
//Loads the Options Panel 
if ( !function_exists( 'optionsframework_init' ) ) {
	define( 'OPTIONS_FRAMEWORK_DIRECTORY', get_template_directory_uri() . '/inc/' );
	require_once dirname( __FILE__ ) . '/inc/options-framework.php';
}
if ( !function_exists( 'of_get_option' ) ) {
function of_get_option($name, $default = false) {
	$optionsframework_settings = get_option('optionsframework');
	// Gets the unique option id
	$option_name = $optionsframework_settings['footer_uploader'];
	$option_name = $optionsframework_settings['phone1'];
	$option_name = $optionsframework_settings['address1'];
	$option_name = $optionsframework_settings['address2'];
	$option_name = $optionsframework_settings['slide1'];
	$option_name = $optionsframework_settings['slide2'];
	$option_name = $optionsframework_settings['slide3'];
	$option_name = $optionsframework_settings['slide4'];
	$option_name = $optionsframework_settings['slide5'];
	$option_name = $optionsframework_settings['slide6'];
	$option_name = $optionsframework_settings['captitle1'];
	$option_name = $optionsframework_settings['captitle2'];
	$option_name = $optionsframework_settings['captitle3'];
	$option_name = $optionsframework_settings['captitle4'];
	$option_name = $optionsframework_settings['captitle5'];
	$option_name = $optionsframework_settings['captitle6'];
	$option_name = $optionsframework_settings['captext1'];
	$option_name = $optionsframework_settings['captext2'];
	$option_name = $optionsframework_settings['captext3'];
	$option_name = $optionsframework_settings['captext4'];
	$option_name = $optionsframework_settings['captext5'];
	$option_name = $optionsframework_settings['captext6'];
	
	if ( get_option($option_name) ) {
		$options = get_option($option_name);
	}
		
	if ( isset($options[$name]) ) {
		return $options[$name];
	} else {
		return $default;
	}
}
}
if (function_exists('register_sidebar')) {
	register_sidebar(array(
		'id' => 'main',
		'name' => 'Main Sidebar',
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget' => '</section>',
		'before_title' => '<h2 class="widgettitle">',
		'after_title' => '</h2>',
	));
	register_sidebar(array(
		'id' => 'category',
		'name' => 'Product Sidebar',
		'description' => 'This is the sidebar for product and category pages',
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget' => '</section>',
		'before_title' => '<h2 class="widgettitle">',
		'after_title' => '</h2>',
	));
}
register_nav_menus( array(
 		'main' => __( 'Main Menu', 'ajbootstrap' ),
		'social' => __( 'Social Media Menu', 'ajbootstrap' ),
		'top' => __( 'Top Navigation Buttons', 'ajbootstrap' ),
		'trailerOptions' => __( 'Trailer Options', 'ajbootstrap' ),
		'quickLinks' => __( 'Quick Links', 'ajbootstrap' ),
		'trailers' => __( 'Trailers', 'ajbootstrap' )
 	) );
// Custom Backend Footer
function bigtex_custom_admin_footer() {
	echo '<span id="footer-thankyou">Developed by <a href="http://bigtextrailers.com" target="_blank">Big Tex Trailers Marketing Team</a></span>.';
}
// adding it to the admin area
add_filter('admin_footer_text', 'bigtex_custom_admin_footer');
// Register Custom Navigation Walker
require_once('wp_bootstrap_navwalker.php');
//enqueue styles and javascript
function stylin(){
	wp_enqueue_style('main', get_bloginfo( 'stylesheet_url', array(), '20140610', 'screen') );
	wp_enqueue_style('bootstrap', get_template_directory_uri().'/css/bootstrap.min.css', '', '', 'screen');
	wp_enqueue_style('fonts', 'https://fonts.googleapis.com/css?family=Open+Sans:400,700|Oswald:700|Lobster', '', '', 'screen');
	wp_enqueue_style('fontawesome', get_template_directory_uri().'/css/font-awesome.min.css', '', '', 'screen');
}
add_action( 'wp_enqueue_scripts', 'stylin' );
function scriptaculous(){
	wp_enqueue_script('bootstrap-js', get_template_directory_uri().'/javascript/bootstrap.min.js', '', '', true);
	wp_enqueue_script('custom', get_template_directory_uri().'/javascript/custom.js', '', '', true);
}
add_action( 'wp_enqueue_scripts', 'scriptaculous' );
// add ie conditional html5 shim to header
function add_ie_html5_shim () {
    echo '<!--[if lt IE 9]>';
    echo '<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>';
    echo '<![endif]-->';
		echo '<!--[if lt IE 9]>';
    echo '<script src="http://bigtextrailers.com/wp-content/themes/bigTexTrailers/javascript/respond.js"></script>';
    echo '<![endif]-->';
}
add_action('wp_head', 'add_ie_html5_shim');
	//login logo
	function my_login_logo() { ?>
    <style type="text/css">
			body.login div#login h1 a {
				background-image:url(<?php echo get_bloginfo( 'template_directory' ) ?>/images/site-login-logo.png);
				background-size:200px 100px;
				height:100px;
				padding-bottom:30px;
				width:200px;
			}
    </style>
<?php }
add_action( 'login_enqueue_scripts', 'my_login_logo' );
function my_login_logo_url() {
    return get_bloginfo( 'url' );
}
add_filter( 'login_headerurl', 'my_login_logo_url' );
function my_login_logo_url_title() {
    return 'Big Tex Trailers';
}
add_filter( 'login_headertitle', 'my_login_logo_url_title' );
//favicon
function theme_favicon() { ?>
	<link rel="shortcut icon" href="<?php echo bloginfo('stylesheet_directory') ?>/images/favicon.png" > 
<?php }
add_action('wp_head', 'theme_favicon');
// customize wp visual editor
function my_mce_buttons( $buttons ){
	unset($buttons[6]);
	unset($buttons[7]);
	unset($buttons[8]);
  return $buttons;
}
add_filter('mce_buttons', 'my_mce_buttons');
function my_mce_buttons_2( $buttons ) {
	array_unshift( $buttons, 'styleselect' );
	return $buttons;
}
add_filter('mce_buttons_2', 'my_mce_buttons_2');
function add_editor_styles(){
    add_editor_style( 'css/editor-styles.css' );
}
add_action( 'init', 'add_editor_styles' );
//edit button on post and pages
function custom_edit_post_link($output) {
 $output = str_replace('class="post-edit-link"', 'class="post-edit-link edit-link btn btn-default btn-block uppercase"', $output);
 return $output;
}
add_filter('edit_post_link', 'custom_edit_post_link');
remove_filter ('acf_the_content', 'wpautop');
wp_enqueue_script('jquery');
?>