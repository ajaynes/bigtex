<div id="homeSlider" class="carousel slide">
	<div class="row">
    <div class="carousel-inner">
      <div class="item active">
        <div class="col-xs-12 col-sm-4">
          <a href="/gooseneck-trailers/"><img src="<?php bloginfo('stylesheet_directory'); ?>/images/slides/GoosenecksSlide.jpg" width="800" height="318" alt="Gooseneck Trailers" class="img-responsive" /></a>
        </div><!--col-xs-12 col-sm-4-->
        <div class="col-xs-12 col-sm-4">
          <a href="/employment/"><img src="<?php bloginfo('stylesheet_directory'); ?>/images/slides/careersBTbutton.jpg" width="800" height="318" alt="Careers at Big Tex" class="img-responsive" /></a>
        </div><!--col-xs-12 col-sm-4-->
        <div class="col-xs-12 col-sm-4">
          <a href="/single-axle-trailers/"><img src="<?php bloginfo('stylesheet_directory'); ?>/images/slides/SingleAxleSlide.jpg" width="800" height="318" alt="Single Axle Trailers" class="img-responsive" /></a>
        </div><!--col-xs-12 col-sm-4-->
      </div><!--item active-->
      <div class="item">
      	<div class="col-xs-12 col-sm-4">
        	<a href="/dump-trailers/"><img src="<?php bloginfo('stylesheet_directory'); ?>/images/slides/DumpSlide.jpg" width="800" height="318" alt="Dump Trailers" class="img-responsive" /></a>
        </div><!--col-xs-12 col-sm-4-->
        <div class="col-xs-12 col-sm-4">
        	<a href="/warranty-center/"><img src="<?php bloginfo('stylesheet_directory'); ?>/images/slides/warantyCenter.jpg" width="800" height="318" alt="Warranty Center" class="img-responsive" /></a>
        </div><!--col-xs-12 col-sm-4-->
        <div class="col-xs-12 col-sm-4">
        	<a href="/gooseneck-trailers/"><img src="<?php bloginfo('stylesheet_directory'); ?>/images/slides/GoosenecksSlide.jpg" width="800" height="318" alt="Gooseneck Trailers" class="img-responsive" /></a>
        </div><!--col-xs-12 col-sm-4-->
      </div><!--item-->
      <div class="item">
      	<div class="col-xs-12 col-sm-4">
        	<a href="/heavy-equipment-trailers/"><img src="<?php bloginfo('stylesheet_directory'); ?>/images/slides/HeavyEquipmentSlide.jpg" width="800" height="318" alt="Heavy Equipment Trailers" class="img-responsive" /></a>
         <!-- <div class="caption">
          	<a href="/marketing/equipment-trailers/">Explore Our Equipment Trailers <span class="fa fa-caret-right"></span></a>
          </div>--><!--caption-->
        </div><!--col-xs-12 col-sm-4-->
        <div class="col-xs-12 col-sm-4">
          <a href="http://bigtexapparel.com/" target="_blank"><img src="<?php bloginfo('stylesheet_directory'); ?>/images/slides/apparelBTbutton.jpg" width="800" height="318" alt="Big Tex Apparel" class="img-responsive" /></a>
          <!--<div class="caption">
          	<a href="#">Ask Big Tex</a>
          </div>--><!--caption-->
        </div><!--col-xs-12 col-sm-4-->
        <div class="col-xs-12 col-sm-4">
        	<a href="/tandem-axle-trailers/"><img src="<?php bloginfo('stylesheet_directory'); ?>/images/slides/TandemAxleSlide.jpg" width="800" height="318" alt="Tandem Axle Trailers" class="img-responsive" /></a>
         <!-- <div class="caption">
          	<a href="/marketing/tandem-axle-trailers/">Explore Our Auto Trailers <span class="fa fa-caret-right"></span></a>
          </div>--><!--caption-->
        </div><!--col-xs-12 col-sm-4-->
      </div><!--item-->
       <div class="item">
      	<div class="col-xs-12 col-sm-4">
        	<a href="/dump-trailers/"><img src="<?php bloginfo('stylesheet_directory'); ?>/images/slides/DumpSlide.jpg" width="800" height="318" alt="Dump Trailers" class="img-responsive" /></a>
          <!--<div class="caption">
          	<a href="/marketing/dump-trailers/">Explore Our Dump Trailers <span class="fa fa-caret-right"></span></a>
          </div>--><!--caption-->
        </div><!--col-xs-12 col-sm-4-->
        <div class="col-xs-12 col-sm-4">
        	<a href="/landscape-trailers/"><img src="<?php bloginfo('stylesheet_directory'); ?>/images/slides/LandscapeSlide.jpg" width="800" height="318" alt="Landscape Trailers" class="img-responsive" /></a>
          <!--<div class="caption">
          	<a href="/marketing/landscape-trailers/">Explore Our Landscape Trailers <span class="fa fa-caret-right"></span></a>
          </div>--><!--caption-->
        </div><!--col-xs-12 col-sm-4-->
        <div class="col-xs-12 col-sm-4">
        	<a href="/automotorcycle-trailers/"><img src="<?php bloginfo('stylesheet_directory'); ?>/images/slides/AutoMotoSlide.jpg" width="800" height="318" alt="Auto Trailers" class="img-responsive" /></a>
          <!--<div class="caption">
          	<a href="/marketing/car-haulers/">Explore Our Auto Trailers <span class="fa fa-caret-right"></span></a>
          </div>--><!--caption-->
        </div><!--col-xs-12 col-sm-4-->
      </div><!--item-->
    </div><!--carousel inner-->
	</div><!--row-->
   <!-- Controls -->
  <a class="left carousel-control hidden-xs" href="#homeSlider" data-slide="prev">
    <span class="fa fa-chevron-left"></span>
  </a>
  <a class="right carousel-control hidden-xs" href="#homeSlider" data-slide="next">
    <span class="fa fa-chevron-right"></span>
  </a>
</div><!--homeSlider-->  

<?php /*?>	<div class="carousel-inner">
    		<div class="item active"><img src="<?php bloginfo('stylesheet_directory'); ?>/images/slides/dumpTrailers.jpg" width="800" height="318" alt="Dump Trailers" /></div>
        <div class="item"><img src="<?php bloginfo('stylesheet_directory'); ?>/images/slides/truck-beds.jpg" width="960" height="351" alt="Dump Trailer" /></div>
        <div class="item"><img src="<?php bloginfo('stylesheet_directory'); ?>/images/slides/stock-trailer.jpg" width="960" height="351" alt="Stock Trailer" /></div>
        <div class="item"><img src="<?php bloginfo('stylesheet_directory'); ?>/images/slides/gooseneck-trailers.jpg" width="960" height="351" alt="Utility Trailers" /></div>
        <div class="item"><img src="<?php bloginfo('stylesheet_directory'); ?>/images/slides/dump-trailer.jpg" width="960" height="351" alt="Gooseneck Trailers" /></div>
        <div class="item"><img src="<?php bloginfo('stylesheet_directory'); ?>/images/slides/bumper-pull-horse-trailer.jpg" width="960" height="351" alt="Truck Beds" /></div>
    </div><?php */?>
