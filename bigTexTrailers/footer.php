</div><!--main-->
</div><!--container-->
<footer>
	<div class="container">
  	<div class="row">
    	<div class="col-xs-12 col-sm-3 col-sm-offset-1 menu">
      	<h4>Trailers</h4>
      	<?php wp_nav_menu(array(
						'theme_location' => 'trailers',
						'container' => 'nav',
						'menu_class' => 'list-unstyled',
						'fallback_cb' => false
					));
				?>
      </div>
      <div class="col-xs-12 col-sm-3 col-sm-offset-1 menu">
      	<h4>Trailer Options</h4>
      	<?php wp_nav_menu(array(
						'theme_location' => 'trailerOptions',
						'container' => 'nav',
						'menu_class' => 'list-unstyled',
						'fallback_cb' => false
					));
				?>
      </div>
      <div class="col-xs-12 col-sm-3 col-sm-offset-1">
      	<h4>Quick Links</h4>
      	<?php wp_nav_menu(array(
						'theme_location' => 'quickLinks',
						'container' => 'nav',
						'menu_class' => 'list-unstyled',
						'fallback_cb' => false
					));
				?>
      </div>
    </div><!--row-->
  	</div><!--container-->      
    	<div class="gray">
      	<div class="container">
        	<div class="row">
        	<div class="col-xs-12 col-md-8">
          	<p class="disclaimer">&copy;<?php echo date('Y'); ?> Copyright <a href="http://www.bigtextrailers.com/" title="Big Tex Trailers" rel="nofollow" target="_blank">Big Tex Trailers</a>. Big Tex Trailers reserves the right to change price, design, material and/or specifications without notice or obligation. All Rights Reserved. All text, images, logos, content, design and coding of this website is protected by all applicable copyright and trademark laws.</p><p><a href="/privacy-policy/" title="View our privacy policy">Privacy Policy</a> <span class="bigTexPower pull-right"><a href="http://www.bigtextrailers.com/" title="Big Tex Trailers Marketing" rel="nofollow" target="_blank">Powered by BigTex Trailers - Marketing</a></span></p>
            </div><!--col-xs-10-->
            <div class="col-xs-12 col-md-4 text-center">
            	<?php wp_nav_menu(array(
						'theme_location' => 'social',
						'container' => 'nav',
						'menu_class' => 'list-inline list-unstyled',
						'fallback_cb' => false
					));
				?>
            </div><!--col-xs-2 center-->
           </div><!--row-->
        </div><!--container-->
        </div><!--gray-->
        <div class="dark">
        <div class="container">
        	<?php if ( of_get_option('footer_uploader') ) { ?>
    				<div class="row">
        			<div class="col-xs-12 text-center">   	 
            		<a href="/brands/"><img src="<?php echo of_get_option('footer_uploader'); ?>" alt="Trailer Brands We Proudly Carry" class="img-responsive" /></a>
            	</div><!--col-xs-12 center-->  
        		</div><!--row-->
        	<?php } ?>
        </div><!--container-->
        </div><!--dark-->
</footer>
<!--browser update-->
<script> 
var $buoop = {c:2}; 
function $buo_f(){ 
 var e = document.createElement("script"); 
 e.src = "//browser-update.org/update.min.js"; 
 document.body.appendChild(e);
};
try {document.addEventListener("DOMContentLoaded", $buo_f,false)}
catch(e){window.attachEvent("onload", $buo_f)}
</script> 

<!--pixel thing-->
<script type="text/javascript">
adroll_adv_id = "7PDXI4TUQFAGFJKDRMOBRX";
adroll_pix_id = "FDGGWORBSRE7TNM6EBWN5S";
// adroll_email = "username@example.com"; // OPTIONAL: provide email to improve user identification
(function () {
var _onload = function(){
if (document.readyState && !/loaded|complete/.test(document.readyState)){setTimeout(_onload, 10);return}
if (!window.__adroll_loaded){__adroll_loaded=true;setTimeout(_onload, 50);return}
var scr = document.createElement("script");
var host = (("https:" == document.location.protocol) ? "https://s.adroll.com" : "http://a.adroll.com");
scr.setAttribute('async', 'true');
scr.type = "text/javascript";
scr.src = host + "/j/roundtrip.js";
((document.getElementsByTagName('head') || [null])[0] ||
document.getElementsByTagName('script')[0].parentNode).appendChild(scr);
};
if (window.addEventListener) {window.addEventListener('load', _onload, false);}
else {window.attachEvent('onload', _onload)}
}());
</script> 
		<?php wp_footer(); ?>

	</body>
</html>