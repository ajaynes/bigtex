<?php
/**
 * The template used for displaying page content
 */
?>
<div class="col-xs-12 bt trailers">
	<div class="row">
  	<div class="col-xs-12 col-sm-6">
    	<?php the_post_thumbnail('full', array('class' => 'img-responsive')); ?>
    </div><!--col-xs-6-->
    <div class="col-xs-12 col-sm-6">
			<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute() ?>" ><?php the_title('<h3>', '</h3>') ?></a>
      <div class="entry-content">
      	<?php the_excerpt();?>
        <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute() ?>" class="btn btn-custom btn-block">Standard Features and Specs <span class="fa fa-chevron-circle-right"></span></a>
      </div><!--entry-content-->
  	</div><!--col-xs-6-->	
  </div><!--row-->
  <?php edit_post_link('<span class="fa fa-pencil"></span> edit'); ?>
</div><!--col-xs-12 bt trailers-->
<div class="clearfix"></div>
<hr class="trailersHR" />